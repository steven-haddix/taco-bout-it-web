import React, { Component } from 'react';
import styled from "styled-components";
import { Box } from '@rebass/grid';
import config from "../../config";
import Bar from '../../components/Bar';
import Timer from '../../components/Timer';

const LeaderboardContainer = styled(Box)`
    background-color: #2B3245;
    color: white;
    padding: 0 0 20px 0;
    max-height: 90vh;
    overflow: hidden;
    
    border-radius: 5px; 
    -moz-border-radius: 5px; 
    -webkit-border-radius: 5px;

    box-shadow: 0px 10px 10px -2px rgba(0,0,0,0.3);
    -webkit-box-shadow: 0px 10px 10px -2px rgba(0,0,0,0.3);
    -moz-box-shadow: 0px 10px 10px -2px rgba(0,0,0,0.3);
`;

const Header = styled.div`
    background: #2B3245;
    box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
    border-radius: 7px 7px 0px 0px;
    font-size: 36px;
    padding: 20px;

    display: flex;
    align-content: center;
    justify-content: space-between;
    position: relative;
`;

const Body = styled.div`
    padding: 0 20px;
    margin: 0px 0 0 0;
`;

const NoTacos = styled.div`
    color: white;
    font-size: 20px;
    padding: 50px 30px;
    text-align: center;
`

const Record = styled.div`
    display: flex;
    flex-direction: row;
    font-size: 16px;
    height: 50px;
`;

const Title = styled.div`
    justify-self: center;
    align-self: center;
    padding: 0 5px 0 0;
    width: 20%;
`;

const BarContainer = styled.div`
    width: 80%;
`;

const BarLabel = styled.div`
    align-self: center;
    height: 100%;
    justify-content: flex-end;
    display: flex;
    text-align: center;
    align-items: center
    padding: 0 10px 0 0;
`;

class Leaderboard extends Component {
    constructor(props) {
        super(props);

        this.state = {
            users: [],
        }
    }

    async componentDidMount() {
        try {
            await this.refreshUsers();
        } catch (ex) {
            console.error(ex);
        }
    }

    render() {
        const { users, leaderScore } = this.state;

        return (
            <LeaderboardContainer {...this.props}>
                <Header>
                    <div></div>
                    <div>Leaderboard</div>
                    <div>
                        <Timer interval={10} callback={() => this.refreshUsers()}/>
                    </div>
                </Header>
                <Body>
                {users.length === 0 &&
                <NoTacos>There don't appear to be any <span aria-label="taco" role="img">🌮</span>'s yet!</NoTacos>
                }
                {users.map((result, idx) => (
                    result ?
                        <Record key={result.userId} className="record">
                            <Title>{result.name}</Title>
                            <BarContainer>
                                <Bar percent={(result.tacoCount)/leaderScore*100} colorIndex={idx}>
                                    <BarLabel>{result.tacoCount}<span aria-label="taco" role="img">🌮</span></BarLabel>
                                </Bar>
                            </BarContainer>
                        </Record>
                    : null
                ))}
                </Body>
            </LeaderboardContainer>
        )
    }

    async refreshUsers() {
        const res = await this.getUsers();
        const users = res.users;

        this.setState({
            users: users,
            ...this.getLeaderDetails(users),
        })
    }

    getLeaderDetails(users) {
        if (!users[0]) {
            return {};
        }

        const leader = users[0];

        return {
            leader,
            leaderScore: leader.tacoCount,
        }
    }

    async getUsers() {
        const res = await fetch(config.usersApi);
        return await res.json();
    }
}

export default Leaderboard;