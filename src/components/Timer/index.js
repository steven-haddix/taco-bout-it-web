import React, {Component} from 'react';
import styled from 'styled-components';

const TimerContainer = styled.div` 
    .countdown {
        position: relative;
        margin: auto;
        height: 40px;
        width: 40px;
        text-align: center;
    }

    .circular-chart {
        position: absolute;
        top: 0;
        right: 0;
        width: 40px;
        height: 40px;
    }
    
    .countdown-number {
        height: 40px;
        display: flex;
        justify-content: center;
        align-items: center;
        font-size: 18px;
    }
    
    .circle-bg {
        fill: none;
        stroke: white;
        stroke-width: 3.8;
    }
    
    .circle {
        fill: none;
        stroke-width: 2.8;
        stroke-linecap: round;
        transition: 1s;
    }
    
    .circular-chart.orange .circle {
        stroke: white;
    }
`

export default class Timer extends Component {
    constructor(props) {
        super(props);
        const interval = props.interval !== undefined ? props.interval : 0;

        this.state = {
            interval,
            currentTime: interval,
            timer: null,
        }
    }

    componentDidMount() {
        if (this.state.interval > 0) {
            const timer = setInterval(() => {
                const {callback, interval} = this.props;
                const {currentTime} = this.state;

                const nextTime = currentTime - 1;
                const complete = nextTime <= 0;

                if (complete && callback) {
                    callback()
                }

                this.setState({
                    currentTime: complete ? interval : nextTime
                });
            }, 1000);

            this.setState({
                timer: timer
            })
        }
    }

    render() {
        const {interval, currentTime} = this.state;

        return (
            <TimerContainer interval={interval}>
                <div className="countdown">
                    <div className="countdown-number">{currentTime}</div>
                    <svg viewBox="0 0 36 36" className="circular-chart orange">
                        <path className="circle"
                              strokeDasharray={`${currentTime/interval*100}, 100`}
                              d="M18 2.0845
                                  a 15.9155 15.9155 0 0 1 0 31.831
                                  a 15.9155 15.9155 0 0 1 0 -31.831"
                        />
                    </svg>
                </div>
            </TimerContainer>
        )
    }
}
