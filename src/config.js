const envPostfix = process.env.REACT_APP_ENV === 'development' ? '-dev' : '';

const config = {
    usersApi: 'https://tacobot-api' + envPostfix + '.stevenhaddix.com/api/users/list',
}

export default config;