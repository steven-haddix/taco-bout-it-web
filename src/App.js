import React, {Component} from 'react';
import styled, { createGlobalStyle } from 'styled-components';
import { Flex } from '@rebass/grid';
import Leaderboard from './features/Leaderboard';

const GlobalStyle = createGlobalStyle`
  body {
    background-color: #65647D;
    font-family: 'Roboto', sans-serif;
  }
`

const Container = styled(Flex)`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    min-height: 100vh;
    margin: 0 auto; 

    @media screen and (min-width: 1400px) {
        max-width: 1200px;
    }
    
    @media screen and (min-width: 1800px) {
        max-width: 1600px;
    }
`;

class App extends Component {
    render() {
        return (
            <React.Fragment>
                <GlobalStyle />
                <Container>
                    <Leaderboard />
                </Container>
            </React.Fragment>
        );
    }
}

export default App;
